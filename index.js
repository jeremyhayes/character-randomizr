
// function to get a random item from an input array
function getRandomChoice(choices) {
    // Math.random returns decimal between 0 and 1
    const rand = Math.random();

    // multiply that by the number of items in the array
    // to scale the random number to cover all items
    const scaled = rand * choices.length;

    // We need an integer, so Math.floor rounds down
    const index = Math.floor(scaled);

    // Now use that (0-based) index to pick an item
    const item = choices[index];

    return item;
}


// array of options for "Alertness"
const choiceAlertness = [
    'Fresh Faced',
    'Tired'
];

// array of options for "Physical Prowess"
const choiceProwess = [
    'Olympian',
    'Paperweight'
];

// array of options for "Courtesy"
const choiceCourtesy = [
    'Customer Service',
    'Rabid Ferret'
];

// array of options for "Intellect"
const choiceIntellect = [
    'Mensa Member',
    'Clueless'
];


// call the function to get a random choice from each array
const selectedAlertness = getRandomChoice(choiceAlertness);
const selectedProwess = getRandomChoice(choiceProwess);
const selectedCourtesy = getRandomChoice(choiceCourtesy);
const selectedIntellect = getRandomChoice(choiceIntellect);


// slap all the answers into a single object
const result = {
    'Alertness': selectedAlertness,
    'Physical Prowess': selectedProwess,
    'Courtesy': selectedCourtesy,
    'Intellect': selectedIntellect
}


// dump the result to the browser's console/devtools
console.log(result);


// "stringify" the result object to something (somewhat) readable
const resultString = JSON.stringify(result, null, 2);

// dump that into the html element with "id=app"
document.getElementById('app').innerText = resultString;
